import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { PRIORITIES } from "src/app/constants/priorities";
import { STATES } from "src/app/constants/states";
import { Task } from "src/app/interfaces/task.interface";
import { TasksService } from "src/app/services/tasks/tasks.service";

@Component({
  selector: "app-task-form",
  template: `
    <form [formGroup]="this.form">
      <div class="title" *ngIf="this.data.action === 'view'">Task</div>
      <div class="title" *ngIf="this.data.action === 'edit'">Edit</div>
      <div class="title" *ngIf="this.data.action === 'add'">Add</div>
      <mat-form-field appearance="fill">
        <mat-label>Title</mat-label>
        <input matInput formControlName="title" />
      </mat-form-field>
      <mat-form-field appearance="fill">
        <mat-label>Description</mat-label>
        <input matInput formControlName="description" />
      </mat-form-field>
      <mat-form-field appearance="fill">
        <mat-label>State</mat-label>
        <mat-select formControlName="status">
          <mat-option *ngFor="let state of this.states" [value]="state">{{
            this.state
          }}</mat-option>
        </mat-select>
      </mat-form-field>
      <mat-form-field appearance="fill">
        <mat-label>Priority</mat-label>
        <mat-select formControlName="priority">
          <mat-option
            *ngFor="let priority of this.priorities"
            [value]="priority"
            >{{ this.priority }}</mat-option
          >
        </mat-select>
      </mat-form-field>
      <mat-form-field>
        <mat-label>Date</mat-label>
        <input matInput [matDatepicker]="picker" formControlName="date" />
        <mat-hint>MM/DD/YYYY</mat-hint>
        <mat-datepicker-toggle
          matIconSuffix
          [for]="picker"
        ></mat-datepicker-toggle>
        <mat-datepicker #picker></mat-datepicker>
      </mat-form-field>
      <div
        mat-dialog-actions
        *ngIf="this.data.action === 'edit' || this.data.action === 'add'"
      >
        <button mat-button (click)="closeModal()">Cancel</button>
        <button
          class="save"
          mat-button
          (click)="this.data.action === 'add' ? saveTask() : editTask()"
          cdkFocusInitial
        >
          Save
        </button>
      </div>
      <div mat-dialog-actions *ngIf="this.data.action === 'view'">
        <button mat-button (click)="closeModal()">Close</button>
      </div>
    </form>
  `,
  styleUrls: ["./task-form.component.scss"],
})
export class TaskFormComponent implements OnInit {
  /** form used to create, view and edit tasks */
  form: FormGroup = this.formBuilder.group({
    id: [""],
    title: ["", [Validators.required]],
    description: ["", [Validators.required]],
    status: ["", [Validators.required]],
    priority: ["", [Validators.required]],
    date: ["", [Validators.required]],
  });
  /** deep clone of tasks states */
  states: string[] = structuredClone(STATES);
  /** deep clone of tasks priorities */
  priorities: string[] = structuredClone(PRIORITIES);
  /** formatted date from form */
  formattedDate: string;

  constructor(
    public dialogRef: MatDialogRef<TaskFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { task: Task; action: string },
    private formBuilder: FormBuilder,
    private tasksService: TasksService
  ) {}

  /**
   * initial function where the form is set if the action is edit or deactivate the form if the action is view
   */
  ngOnInit(): void {
    this.setData();
    if (this.data.action === "view") this.form.disable();
  }

  /**
   * close dialog or modal
   */
  closeModal() {
    this.dialogRef.close();
  }

  /**
   * set data on the form
   */
  setData() {
    this.form.patchValue(this.data.task);
  }

  /**
   * call tasks service to save the task if the form is valid and close dialog or modal
   */
  saveTask() {
    if (this.form.valid) {
      this.tasksService.newTask(this.form.value);
      this.closeModal();
    }
  }

  /**
   * call tasks service to edit the task if the form is valid and close dialog or modal
   */
  editTask() {
    if (this.form.valid) {
      this.tasksService.updateTask(this.form.value);
      this.closeModal();
    }
  }
}
