import { Component } from "@angular/core";

@Component({
  selector: "app-home",
  template: `<div class="title">Task manager</div>
    <div class="table">
      <app-task-table></app-task-table>
    </div>`,
  styleUrls: ["./home.component.scss"],
})
export class HomeComponent {}
