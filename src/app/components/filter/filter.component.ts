import { Component, EventEmitter, Input, Output, OnInit } from "@angular/core";
import { Task } from "src/app/interfaces/task.interface";

@Component({
  selector: "app-filter",
  template: `<div class="filter">
    <div class="filter__by-status">
      <mat-label>Status:</mat-label>
      <mat-radio-group
        [(ngModel)]="selectedStatus"
        (change)="applyFilter()"
        [disabled]="this.tasks.length === 0"
      >
        <mat-radio-button value="New">New</mat-radio-button>
        <mat-radio-button value="In progress">In progress</mat-radio-button>
        <mat-radio-button value="Completed">Completed</mat-radio-button>
      </mat-radio-group>
    </div>
    <div class="filter__by-priority">
      <mat-label>Priority:</mat-label>
      <mat-radio-group
        [(ngModel)]="selectedPriority"
        (change)="applyFilter()"
        [disabled]="this.tasks.length === 0"
      >
        <mat-radio-button value="Low">Low</mat-radio-button>
        <mat-radio-button value="Medium">Medium</mat-radio-button>
        <mat-radio-button value="High">High</mat-radio-button>
      </mat-radio-group>
    </div>
    <div class="filter__clear-button">
      <button
        mat-raised-button
        color="basic"
        (click)="clearSelection()"
        [disabled]="this.tasks.length === 0"
      >
        Clear
      </button>
    </div>
  </div>`,
  styleUrls: ["./filter.component.scss"],
})
export class FilterComponent {
  /** length of the array containing the tasks */
  @Input() lenght: Task[] = [];
  /** array of tasks */
  @Input() tasks: Task[] = [];
  /** state selected to apply the filter */
  @Input() selectedStatus: string | null;
  /** priority selected to apply the filter */
  @Input() selectedPriority: string | null;
  /** filtered tasks to be output for viewing */
  @Output() filteredTasks: EventEmitter<Task[]> = new EventEmitter<Task[]>();

  /**
   * returns the filtered tasks and outputs them for viewing
   * when statusMatch and priorityMatch are true, returns the tasks
   * emit the filtered tasks
   */
  applyFilter(): void {
    const filteredTasks = this.tasks.filter((task: Task) => {
      const statusMatch =
        !this.selectedStatus || task.status === this.selectedStatus;
      const priorityMatch =
        !this.selectedPriority || task.priority === this.selectedPriority;
      return statusMatch && priorityMatch;
    });
    this.filteredTasks.emit(filteredTasks);
  }

  /**
   * set selectedPriority and selectedStatus to null to clear the selection and return to view all tasks
   * emit all tasks
   */
  clearSelection(): void {
    this.selectedPriority = null;
    this.selectedStatus = null;
    this.filteredTasks.emit(this.tasks);
  }
}
