import { Component, OnInit, ViewChild } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { MatTableDataSource } from "@angular/material/table";
import { Task } from "src/app/interfaces/task.interface";
import { TasksService } from "src/app/services/tasks/tasks.service";
import Swal from "sweetalert2";
import { TaskFormComponent } from "../task-form/task-form.component";
import { DISPLAYED_COLUMNS } from "src/app/constants/displayed-columns";
import { MatSort } from "@angular/material/sort";

@Component({
  selector: "app-task-table",
  template: ` <app-filter
      [tasks]="this.tasks"
      [selectedPriority]="this.selectedPriority"
      [selectedStatus]="this.selectedStatus"
      (filteredTasks)="this.filteredTasks($event)"
    ></app-filter>
    <table
      mat-table
      [dataSource]="dataSource"
      class="mat-elevation-z8"
      matSort
      matSortActive="date"
      matSortDirection="desc"
    >
      <tr class="mat-row" *matNoDataRow>
        <td class="no-tasks-to-show" colspan="5">
          There are no tasks to display, start adding a new one.
        </td>
      </tr>
      <ng-container matColumnDef="title">
        <th mat-header-cell *matHeaderCellDef>Title</th>
        <td mat-cell *matCellDef="let task">
          {{ task.title }}
        </td>
      </ng-container>

      <ng-container matColumnDef="status">
        <th mat-header-cell *matHeaderCellDef>Status</th>
        <td
          mat-cell
          *matCellDef="let task"
          [ngClass]="task.status === 'Completed' ? 'completed' : 'incompleted'"
        >
          {{ task.status }}
        </td>
      </ng-container>

      <ng-container matColumnDef="priority">
        <th mat-header-cell *matHeaderCellDef mat-sort-header>Priority</th>
        <td mat-cell *matCellDef="let task">{{ task.priority }}</td>
      </ng-container>

      <ng-container matColumnDef="date">
        <th mat-header-cell *matHeaderCellDef mat-sort-header>Date</th>
        <td mat-cell *matCellDef="let task">
          {{ task.date | date : "dd/MM/yyyy" }}
        </td>
      </ng-container>

      <ng-container matColumnDef="actions">
        <th mat-header-cell *matHeaderCellDef>Actions</th>
        <td mat-cell *matCellDef="let task">
          <div class="actions">
            <button
              class="view button"
              mat-mini-fab
              color="primary"
              (click)="this.viewTask(task, 'view')"
            >
              <mat-icon>remove_red_eye</mat-icon>
            </button>
            <button
              class="edit button"
              mat-mini-fab
              color="primary"
              (click)="editTask(task, 'edit')"
            >
              <mat-icon>edit</mat-icon>
            </button>
            <button
              class="delete button"
              mat-mini-fab
              color="warn"
              (click)="showModalDeleteTask(task)"
            >
              <mat-icon>delete</mat-icon>
            </button>
          </div>
        </td>
      </ng-container>

      <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
      <tr mat-row *matRowDef="let row; columns: displayedColumns"></tr>
    </table>
    <div class="general-actions">
      <div class="add generla-button">
        <button mat-raised-button color="primary" (click)="addTask('add')">
          Add
        </button>
      </div>
      <div class="mark-as-completed generla-button">
        <button
          [disabled]="this.tasks.length === 0"
          mat-raised-button
          color="primary"
          (click)="this.markAllAsCompleted()"
        >
          Mark all as completed
        </button>
      </div>
      <div class="delete-all generla-button">
        <button
          mat-raised-button
          color="warn"
          (click)="showModalDeleteAll()"
          [style.backgroundColor]="'#FF0000'"
          [disabled]="this.tasks.length === 0"
        >
          Delete all
        </button>
      </div>
    </div>`,
  styleUrls: ["./task-table.component.scss"],
})
export class TaskTableComponent implements OnInit {
  /** obtain the matSort reference to sort data */
  @ViewChild(MatSort) sort: MatSort;
  /** deep clone of the displayed columns in the table */
  displayedColumns: string[] = structuredClone(DISPLAYED_COLUMNS);
  /** data source to displayed in the table */
  dataSource: any;
  /** array of tasks */
  tasks: Task[] = [];
  /** selected state to filter and clear selection */
  selectedStatus: string | null;
  /** selected priority to filter and clear selection */
  selectedPriority: string | null;

  constructor(private tasksService: TasksService, public dialog: MatDialog) {}

  /**
   * initial function where you get the tasks to display
   */
  ngOnInit(): void {
    this.getTasks();
  }

  /**
   * last function to be executed to reference the matSort in the DOM to be able to sort the tasks
   */
  ngAfterViewInit() {
    this.loadDataSource(this.tasks);
  }

  /** open dialog to add a new task
   * @param {string} action - action to perform
   */
  addTask(action: string) {
    this.dialog.open(TaskFormComponent, {
      data: { action },
      disableClose: true,
      hasBackdrop: true,
      width: "400px",
      height: "550px",
    });
  }

  /**
   * open dialog to edit an exist task
   * @param {Task} task - task to edit
   * @param {string} action - action to perform
   */
  editTask(task: Task, action: string) {
    this.dialog.open(TaskFormComponent, {
      data: { task, action },
      disableClose: true,
      hasBackdrop: true,
      width: "400px",
      height: "550px",
    });
  }

  /**
   * open dialog to view an exist task
   * @param {Task} task - task to view
   * @param {string} action - action to perform
   */
  viewTask(task: Task, action: string) {
    this.dialog.open(TaskFormComponent, {
      data: { task, action },
      disableClose: true,
      hasBackdrop: true,
      width: "400px",
      height: "550px",
    });
  }

  /**
   * call service tasks to get the tasks and load them into the table
   */
  getTasks() {
    this.tasksService.getTasks().subscribe((res) => {
      this.tasks = res;
      this.loadDataSource(this.tasks);
    });
  }

  /**
   * create MatTable to show on the page and enable sorting
   * @param {Task[]} tasks - tasks to show
   */
  loadDataSource(tasks: Task[]) {
    this.dataSource = new MatTableDataSource<Task>(tasks);
    this.dataSource.sort = this.sort;
  }

  /**
   * open confirmation modal to delete a task
   * if the result is confirmed, the task is deleted and the success modal is opened
   * @param {Task} task - task to delete
   */
  showModalDeleteTask(task: Task) {
    Swal.fire({
      title: "Are you sure?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it",
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteTask(task);
        Swal.fire("Deleted!", "All your tasks have been deleted", "success");
      }
    });
  }

  /**
   * open confirmation modal to delete all tasks
   * if the result is confirmed, all tasks are deleted and the success modal is opened
   */
  showModalDeleteAll() {
    Swal.fire({
      title: "Are you sure you want to delete all tasks?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it",
    }).then((result) => {
      if (result.isConfirmed) {
        this.deleteAll();
        Swal.fire("Deleted!", "Your task has been deleted.", "success");
      }
    });
  }

  /**
   * function to receive the filtered tasks from the child component and display them in the table
   * @param {Task[]} tasks - tasks to show in the table
   */
  filteredTasks(tasks: Task[]) {
    this.loadDataSource(tasks);
  }

  /**
   * call the task service to mark all tasks as completed
   */
  markAllAsCompleted() {
    this.tasksService.markAllTasksAsCompleted();
  }

  /**
   * set selectedPrioritya and selectedStatus to null to clear filter
   */
  clearFilter() {
    this.selectedPriority = null;
    this.selectedStatus = null;
  }

  /**
   * call the task service to delete task and clear filter
   * @param {Task} task - task to delete
   */
  deleteTask(task: Task) {
    this.tasksService.deleteTask(task.id);
    this.clearFilter();
  }

  /**
   * clear filter and call the task service to delete all tasks
   */
  deleteAll() {
    this.clearFilter();
    this.tasksService.deteleAll();
  }
}
