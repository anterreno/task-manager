import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { AngularMaterialModule } from "./angular-material.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TaskTableComponent } from "./components/task-table/task-table.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FilterComponent } from "./components/filter/filter.component";
import { TaskFormComponent } from "./components/task-form/task-form.component";
import { HomeComponent } from "./components/home/home.component";

@NgModule({
  declarations: [
    AppComponent,
    TaskTableComponent,
    FilterComponent,
    TaskFormComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularMaterialModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [AngularMaterialModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
