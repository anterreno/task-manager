/**
 * definition of structure of a task
 */
export interface Task {
  id: number;
  title: string;
  description: string;
  status: string;
  priority: string;
  date: string;
}
