import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Task } from "src/app/interfaces/task.interface";

@Injectable({
  providedIn: "root",
})
export class TasksService {
  /** key of the storage to save the tasks */
  private storageKey: string = "tasks";
  /** observable to keep the task list up to date */
  private tasksSubject: BehaviorSubject<Task[]> = new BehaviorSubject<Task[]>(
    []
  );
  /** array of the tasks */
  tasks: Task[] = [];
  /** counter of ids to be assigned to new tasks */
  private taskIdCounter = 0;

  constructor() {
    this.loadTasks();
  }

  /**
   * load tasks to the storage
   * if there are stored tasks, converts them to object and assigns them to the tasks array
   * emit the stored tasks
   */
  private loadTasks(): void {
    const storedTasks = localStorage.getItem(this.storageKey);
    if (storedTasks) {
      this.tasks = JSON.parse(storedTasks);
      this.tasksSubject.next(this.tasks);
    }
  }

  /**
   * saves the tasks in the storage, converts them from object to string to save them
   */
  private _saveTasks(): void {
    localStorage.setItem(this.storageKey, JSON.stringify(this.tasks));
  }

  /**
   * @returns {BehaviorSubject} - observable to get the tasks
   */
  getTasks(): BehaviorSubject<Task[]> {
    return this.tasksSubject;
  }

  /**
   * assign the id to the task by calling the private method _generateUniqueId
   * insert the task into the task array
   * emit the new tasks
   * save the tasks in the storage
   * @param {Task} task - task to create
   */
  newTask(task: Task): void {
    const newTask: Task = { ...task, id: this._generateUniqueId() };
    this.tasks.push(newTask);
    this.tasksSubject.next(this.tasks);
    this._saveTasks();
  }

  /**
   * search for the index of the task to be edited
   * if the index is -1, the task does not exist. If it is different from -1, it exists
   * assigns the edited task to the task corresponding to the index searched
   * emit the new tasks
   * save the tasks in the storage
   * @param {Task} task - task to edit
   */
  updateTask(task: Task): void {
    const index = this.tasks.findIndex((t) => t.id === task.id);
    if (index !== -1) {
      this.tasks[index] = task;
      this.tasksSubject.next(this.tasks);
      this._saveTasks();
    }
  }

  /**
   * runs through all the tasks and assigns the completed status to each of them
   * emit the new tasks
   * save the tasks in the storage
   * */
  markAllTasksAsCompleted(): void {
    this.tasks.forEach((task) => {
      task.status = "Completed";
    });
    this.tasksSubject.next(this.tasks);
    this._saveTasks();
  }

  /**
   * search for the index of the task to be deleted
   * if the index is -1, the task does not exist. If it is different from -1, it exists
   * delete the task corresponding to the searched index
   * emit the new tasks
   * save the tasks in the storage
   * @param {number} id - id of the task to delete
   */
  deleteTask(id: number): void {
    const index = this.tasks.findIndex((task) => task.id === id);
    if (index !== -1) {
      this.tasks.splice(index, 1);
      this.tasksSubject.next(this.tasks);
      this._saveTasks();
    }
  }
  /**
   * empties the task array
   * emit the new tasks
   * save the tasks in the storage
   */
  deteleAll() {
    this.tasks = [];
    this.tasksSubject.next(this.tasks);
    this._saveTasks();
  }

/**
 * generate uniques id to the new tasks
 * @returns {number} this.taskIdCounter - number of the new id  
 */
  private _generateUniqueId(): number {
    this.taskIdCounter++;
    return this.taskIdCounter;
  }
}
