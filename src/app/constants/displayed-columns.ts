/**
 * columns to displayed in the table
 */
export const DISPLAYED_COLUMNS = [
  "title",
  "status",
  "priority",
  "date",
  "actions",
];
