/**
 * states of a task
 */
export const STATES = ["New", "In progress", "Completed"];
