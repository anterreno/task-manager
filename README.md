## Getting started

- npm install
- npm start

## Summary

Task manager is a web application in which you can manage your tasks.
Actions you can do with it:

- Add new tasks by entering a title, description, status, priority and date.
- You can view, edit and delete each of the added tasks.
- You can delete all tasks.
- You can mark all tasks as 'Completed'.
- You can filter them by status and priority. And also, you can sort them by status and date.

Storage is used to store all the tasks and to have a persistence of them.

## Stack

- Angular 15
- Angular Material
